Requestum Test project
---
***
Created by Oleg Kreminsky seorusus@gmail.com

## Test Cases

***

### Test Case 1: Sending Email to User for Nodes Located "Outside"

**Objective:** Verify that the EmailService sends an email to the user who created the node, for nodes with `field_location` set to "outside".

**Preconditions:**
- Drupal instance running with `requestum_test` module enabled.
- At least one user account (other than admin) exists.
- Mail system is configured for testing (e.g., using Devel Mail Logger or similar).

**Steps:**
1. Log in as a non-admin user.
2. Create a new node of type `BigEntity` with `field_location` set to "outside".
3. Wait for the next cron run or manually run cron.
4. Check the designated mail log or inbox for a new email related to the created node.

**Expected Result:**
- An email is received by the user who created the node.
- The email contains a message indicating it's a test email for the specific entity created.

---

### Test Case 2: Sending Email to User and Admin for Nodes Located "Inside"

**Objective:** Verify that the EmailService sends an email to both the user who created the node and the admin, for nodes with `field_location` set to "inside".

**Preconditions:**
- Same as Test Case 1.

**Steps:**
1. Log in as a non-admin user.
2. Create a new node of type `BigEntity` with `field_location` set to "inside".
3. Wait for the next cron run or manually run cron.
4. Check the designated mail log or inbox for new emails related to the created node.
5. Verify that both the creating user and the admin have received emails.

**Expected Result:**
- An email is received by both the user who created the node and the admin.
- The user's email contains a message indicating it's a test email for the specific entity created.
- The admin's email contains a message indicating it's a test email for the specific entity created, with possibly different content intended for the admin.

---

### Test Case 3: No Email Sent for Nodes Already Processed

**Objective:** Ensure no emails are sent for nodes that have already been marked as processed (`field_processed` is TRUE).

**Preconditions:**
- Same as Test Case 1.

**Steps:**
1. Log in as any user.
2. Create a new node of type `BigEntity`, with either `field_location` set to "inside" or "outside". Ensure `field_processed` is set to TRUE either programmatically or through UI if available.
3. Run cron.
4. Check the designated mail log or inbox for any new emails related to the created node.

**Expected Result:**
- No new emails related to the node are sent out since it was marked as processed at creation.

---
