<?php

/**
 * Implements hook_cron() for the requestum_test module.
 */
function requestum_test_cron(): void {
  $current_time = \Drupal::time()->getRequestTime();
  $yesterday = $current_time - 86400; // 24 hours.

  // Prepare the query to load BigEntity nodes created within the last day and not processed yet.
  $query = \Drupal::entityQuery('node')
    ->condition('type', 'big_entity')
    ->condition('field_time_created', $yesterday, '>=')
    ->condition('field_processed', FALSE)
    ->accessCheck(TRUE)
    ->range(0, 50); // Adjust the range to manage performance.

  $entity_ids = $query->execute();

  // Load and process each entity using the NodeProcessorService.
  if (!empty($entity_ids)) {
    $nodeProcessor = \Drupal::service('requestum_test.node_processor');
    foreach ($entity_ids as $entity_id) {
      $entity = \Drupal::entityTypeManager()->getStorage('node')->load($entity_id);
      if ($entity) {
        $nodeProcessor->processEntity($entity);
      }
    }
  }
}
