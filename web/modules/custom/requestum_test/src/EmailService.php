<?php

namespace Drupal\requestum_test;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class EmailService.
 *
 * Provides functionality to send emails for the requestum_test module.
 */
class EmailService {

  /**
   * The mail manager service.
   *
   * @var MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The config factory service.
   *
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The current user service.
   *
   * @var AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The logger service.
   *
   * @var LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new EmailService object.
   *
   * @param MailManagerInterface $mailManager
   *   The mail manager service.
   * @param ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param AccountProxyInterface $currentUser
   *   The current user service.
   * @param LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory.
   */
  public function __construct(
    MailManagerInterface $mailManager,
    ConfigFactoryInterface $configFactory,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerFactory
  ) {
    $this->mailManager = $mailManager;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->logger = $loggerFactory->get('requestum_test');
  }

  /**
   * Sends an email.
   *
   * Determines the recipient based on the provided type and sends an email
   * using the Drupal mail system.
   *
   * @param EntityInterface $node
   *   The node entity that is the subject of the email.
   * @param string $recipient_type
   *   The type of recipient ('user' or 'admin') to determine the email recipient.
   */
  public function sendEmail(EntityInterface $node, string $recipient_type): void {
    $module = 'requestum_test';
    $key = $recipient_type === 'admin' ? 'notify_admin' : 'notify_user';
    $site_mail = $this->configFactory->get('system.site')->get('mail');
    $to = $recipient_type === 'admin' ? $site_mail : $node->getOwner()->getEmail();

    if (empty($to)) {
      $this->logger->error('Email address for @type not found.', ['@type' => $recipient_type]);
      return;
    }

    $params = ['message' => 'This is a test email for entity ' . $node->id()];
    $langcode = $this->currentUser->getPreferredLangcode();

    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params);

    if ($result['result'] !== TRUE) {
      $this->logger->error('Failed to send email to @type for node @nid.', ['@type' => $recipient_type, '@nid' => $node->id()]);
    } else {
      $this->logger->notice('Email sent to @type for node @nid.', ['@type' => $recipient_type, '@nid' => $node->id()]);
    }
  }
}
