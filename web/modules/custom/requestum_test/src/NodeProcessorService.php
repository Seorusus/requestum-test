<?php

namespace Drupal\requestum_test;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Provides a service for processing Node entities.
 */
class NodeProcessorService {

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger service.
   *
   * @var LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The email service.
   *
   * @var \Drupal\requestum_test\EmailService
   */
  protected EmailService $emailService;

  /**
   * Constructs a new NodeProcessorService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\requestum_test\EmailService $emailService
   *   The email service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerFactory, EmailService $emailService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerFactory->get('requestum_test');
    $this->emailService = $emailService;
  }

  /**
   * Processes entities based on their location field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   */
  public function processEntity(EntityInterface $entity): void {
    $location = $entity->get('field_location')->value;
    $entity_id = $entity->id();

    // Log and perform actions based on the location.
    if ($location === 'outside') {
      $this->processOutside($entity);
    } elseif ($location === 'inside') {
      $this->processInside($entity);
    }

    // Mark the entity as processed.
    $entity->set('field_processed', TRUE)->save();
    $this->logger->notice('Processed entity @id', ['@id' => $entity_id]);
  }

  /**
   * Handles processing for entities located outside.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   */
  protected function processOutside(EntityInterface $entity): void {
    $this->logger->notice('Processed entity @id with outside location', ['@id' => $entity->id()]);
    // Sends an email to the user.
    $this->emailService->sendEmail($entity, 'user');
  }

  /**
   * Handles processing for entities located inside.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   */
  protected function processInside(EntityInterface $entity): void {
    $this->logger->notice('Processed entity @id with inside location', ['@id' => $entity->id()]);
    // Sends emails to both the user and admin.
    $this->emailService->sendEmail($entity, 'user');
    $this->emailService->sendEmail($entity, 'admin');
  }
}
